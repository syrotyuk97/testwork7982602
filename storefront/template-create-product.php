<?php
/**
 * Template to display a form to create a product
 *
 * Template Name: CREATE PRODUCT
 *
 * @package storefront
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="entry-header">
                    <?php
                    storefront_post_thumbnail( 'full' );
                    the_title( '<h1 class="entry-title">', '</h1>' );
                    ?>
                </header><!-- .entry-header -->
                <div class="entry-content">
                    <form id="test_work_create_product" method="post" enctype="multipart/form-data">
                        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                            <label for="product_name">Product name:<span class="required">*</span></label>
                            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="product_name" id="product_name">
                        </p>
                        <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                            <label for="product_price">Product price:<span class="required">*</span></label>
                            <input type="number" class="woocommerce-Input woocommerce-Input--text input-text" name="product_price" id="product_price">
                        </p>
                        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                            <label for="product_type">Product type:<span class="required">*</span></label>
                            <select name="product_type" id="product_type" class="woocommerce-Input woocommerce-Input--text input-text">
                                <option value="">Please select</option>
                                <option value="rare">Rare</option>
                                <option value="frequent">Frequent</option>
                                <option value="unusual">Unusual</option>
                            </select>
                        </p>
                        <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                            <label for="product_date">Product date:<span class="required">*</span></label>
                            <input type="date" class="woocommerce-Input woocommerce-Input--text input-text" name="product_date" id="product_date">
                        </p>
                        <p class="woocommerce-form-row form-row ">
                            <label for="product_image">Product image:<span class="required">*</span></label>
                            <input type="file" class="woocommerce-Input woocommerce-Input--text input-text" name="product_image" id="product_image">
                        </p>
                        <button type="submit" class="woocommerce-Button button" >Create Product</button>
                    </form>
                </div><!-- .entry-content -->
            </article>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
