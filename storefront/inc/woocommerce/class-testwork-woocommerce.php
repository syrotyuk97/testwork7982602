<?php
/**
 * Class for test task
 *
 * @package  storefront
 * @since    2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Test_Work_Class' ) ) {

    class Test_Work_Class{

        /**
         * Setup class.
         *
         * @since 2.4.0
         * @return void
         */
        public function __construct() {
            add_filter('woocommerce_product_data_tabs', array( $this, 'test_work_product_data_tab' ) );
            add_action( 'admin_head', array( $this, 'test_work_custom_style' ) );
            add_action( 'admin_head', array( $this, 'test_work_custom_script' ) );
            add_action( 'woocommerce_product_data_panels', array( $this, 'test_work_product_data_panel' ) );
            add_action( 'woocommerce_process_product_meta', array( $this,'test_work_save_meta'));
            add_action( 'wp_ajax_nopriv_test_work_action_create_product', array( $this,'test_work_action_create_product'));
            add_action( 'wp_ajax_test_work_action_create_product', array( $this,'test_work_action_create_product'));
            add_action( 'wp_footer', array( $this,'test_work_custom_script_form'));
        }

        /**
         * Return array of tabs to show.
         *
         * @since 2.4.3
         *
         * @return array
         */
        public function test_work_product_data_tab( $tabs ){

            $tabs['test_work'] = array(
                'label'    => 'Test Work',
                'target'   => 'test_work_product_data',
                'class'    => array('test_work'),
                'priority' => 80,
            );
            return $tabs;

        }

        /**
         * Set custom styles
         *
         * @since 2.4.3
         */
        public function test_work_custom_style() {

            ?><style>
                #woocommerce-product-data ul.wc-tabs li.test_work_options a:before { content: '\f118'; }

                .test_work_image_link{
                    width: 150px;
                    height: 150px;
                }
                .test_work_image_field{
                    display: flex;
                    align-items: center;
                }
                .test_work_image_field .description{
                    margin: 0 0 10px 0;
                    display: block;
                }
                .test_work_image_buttons{
                    margin-left: 25px;
                }
            </style>
            <?php

        }

        /**
         * Set custom scripts
         *
         * @since 2.4.3
         */
        public function test_work_custom_script() {

            ?>
            <script>


                jQuery(document).ready(function($){

                    let custom_uploader;
                    $('#test_work_image_button_upload').click(function(e) {

                        e.preventDefault();

                        //If the uploader object has already been created, reopen the dialog
                        if (custom_uploader) {
                            custom_uploader.open();
                            return;
                        }

                        //Extend the wp.media object
                        custom_uploader = wp.media.frames.file_frame = wp.media({
                            title: 'Choose a Image',
                            button: {
                                text: 'Choose a Image'
                            },
                            multiple: false
                        });

                        //When a file is selected, grab the URL and set it as the text field's value
                        custom_uploader.on('select', function() {
                            attachment = custom_uploader.state().get('selection').first().toJSON();
                            $('#test_work_image').val(attachment.id);
                            $('.test_work_image_link').attr('src',attachment.url);
                            $('#test_work_image_button_upload').toggleClass('hidden');
                            $('#test_work_image_button_remove').toggleClass('hidden');
                        });

                        //Open the uploader dialog
                        custom_uploader.open();

                    });

                    $('#test_work_image_button_remove').click(function (e) {
                        e.preventDefault();

                        let remove_image = confirm("Are you sure you want to delete the image?")

                        if(remove_image){
                            $('#test_work_image').val('');
                            $('.test_work_image_link').attr('src','<?php echo wc_placeholder_img_src(); ?>');
                            $('#test_work_image_button_upload').toggleClass('hidden');
                            $('#test_work_image_button_remove').toggleClass('hidden');
                        }
                    });

                    $('#test_work_image_button_clear').click(function (e) {
                        e.preventDefault();

                        let clear_all = confirm("Are you sure you want to clear all fields?")

                        if(clear_all) {

                            $('#test_work_image').val('');
                            $('#test_work_type').val('');
                            $('#test_work_date').val('');
                            $('.test_work_image_link').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
                            $('#test_work_image_button_upload').removeClass('hidden');
                            $('#test_work_image_button_remove').addClass('hidden');
                        }
                    });

                    $('#test_work_image_button_submit').click(function (e) {
                        e.preventDefault();

                        $( "#publish" ).trigger( "click" );
                    });

                });
            </script>
            <?php

        }

        /**
         * Content test work tab
         *
         * @since 2.4.3
         */
        public function test_work_product_data_panel(){

            echo '<div id="test_work_product_data" class="panel woocommerce_options_panel hidden">';

            $attach_id = get_post_meta( get_the_ID(), 'test_work_image', true );
            $button_upload_class = "hidden";
            $button_remove_class = "";

            if($attach_id == "") {
                $image = wc_placeholder_img_src();

                $button_upload_class = "";
                $button_remove_class = "hidden";
            }
            else{
                $image = wp_get_attachment_image_url($attach_id,'thumbnail');
            }

            echo '<p class="form-field test_work_image_field ">
                    <label>Image</label>
                    <img class="test_work_image_link" src="' . $image . '" >
                    <span class="test_work_image_buttons">
                        <span class="description">This image will replace the main image</span>
                        <input type="button" class="'.$button_upload_class.'" id="test_work_image_button_upload" value="Upload image" placeholder="">
                        <input type="button" class="'.$button_remove_class.'" id="test_work_image_button_remove" value="Remove image" placeholder="">
                    </span>
                    
             </p>';

            woocommerce_wp_text_input( array(
                'id'                => 'test_work_image',
                'value'             => get_post_meta( get_the_ID(), 'test_work_image', true ),
                'type'              => 'hidden',
            ) );

            woocommerce_wp_text_input( array(
                'id'          => 'test_work_date',
                'value'       => get_post_meta( get_the_ID(), 'test_work_date', true ),
                'label'       => 'Date',
                'type'        => 'date',
            ) );

            woocommerce_wp_select( array(
                'id'          => 'test_work_type',
                'value'       => get_post_meta( get_the_ID(), 'test_work_type', true ),
                'label'       => 'Product type',
                'options'     => array( '' => 'Please select', 'rare' => 'Rare', 'frequent' => 'Frequent', 'unusual' => 'Unusual'),
            ) );

            echo '<p class="form-field test_work_footer_buttons">
                <input type="button" class="button button-large button-secondary" id="test_work_image_button_clear" value="Clear custom fields" placeholder="">
                <input type="button" class="button button-primary button-large" id="test_work_image_button_submit" value="Submit all fields" placeholder="">
            </p>';

            echo '</div>';

        }

        /**
         * Set custom scripts to form
         *
         * @since 2.4.3
         */
        public function test_work_custom_script_form() {

            ?>
            <script>


                jQuery(document).ready(function($){
                    jQuery('#test_work_create_product').on('submit', function(e){
                        e.preventDefault();

                        form_data = new FormData();
                        form_data.append('action', 'test_work_action_create_product');
                        form_data.append('product_image', $('#product_image').prop('files')[0]);
                        form_data.append('product_name', $('#product_name').val());
                        form_data.append('product_price', $('#product_price').val());
                        form_data.append('product_date', $('#product_date').val());
                        form_data.append('product_type', $('#product_type').val());

                        $.ajax({
                            url: location.origin + '/wp-admin/admin-ajax.php',
                            type: 'post',
                            data: form_data,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            error : function(response){
                                console.log(response);
                            },
                            success : function(data){
                                alert('Product created!')
                            }
                        });
                    });

                });
            </script>
            <?php

        }


        /**
         * Create product with ajax and action
         *
         * @since 2.4.3
         */
        public function test_work_action_create_product(){

            if( ! isset( $_FILES ) || empty( $_FILES ) || ! isset( $_POST ) )
                return;

            if ( ! function_exists( 'wp_handle_upload' ) ) {
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
            }

            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            $upload_overrides = array( 'test_form' => false );

            $image = $_FILES['product_image'];

            if ($image['name']) {
                $uploadedfile = array(
                    'name'     => $image['name'],
                    'type'     => $image['type'],
                    'tmp_name' => $image['tmp_name'],
                    'error'    => $image['error'],
                    'size'     => $image['size']
                );

                $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );

                if ( $movefile && !isset( $movefile['error'] ) ) {

                    $attachment = array(
                        'post_mime_type' => $movefile['type'],
                        'post_title' => sanitize_file_name( $image['name'] ),
                        'post_content' => '',
                        'post_status' => 'inherit'
                    );

                    $attach_id = wp_insert_attachment( $attachment, $movefile['file'] );

                    $attach_data = wp_generate_attachment_metadata( $attach_id, $movefile['file'] );
                    wp_update_attachment_metadata( $attach_id, $attach_data );
                }

                if($attach_id){
                    $product_array = array(
                        'post_status' => "publish",
                        'post_title' => $_POST['product_name'],
                        'post_parent' => '',
                        'post_type' => "product",
                    );

                    $product_id = wp_insert_post( $product_array );

                    if($product_id){
                        add_post_meta($product_id, '_thumbnail_id', $attach_id);

                        wp_set_object_terms( $product_id, 'simple', 'product_type');

                        update_post_meta( $product_id, '_sale_price', $_POST['product_price'] );
                        update_post_meta( $product_id, '_regular_price', $_POST['product_price'] );
                        update_post_meta( $product_id, 'test_work_image', $attach_id );
                        update_post_meta( $product_id, 'test_work_date', $_POST['product_date'] );
                        update_post_meta( $product_id, 'test_work_type', $_POST['product_type'] );
                    }
                }
            }
        }

        /**
         * Save custom fields
         *
         * @since 2.4.3
         */
        public function test_work_save_meta( $post_id ){

            if( isset( $_POST['test_work_image'] ) ) {
                update_post_meta( $post_id, 'test_work_image', $_POST['test_work_image'] );
                set_post_thumbnail($post_id,$_POST['test_work_image']);
            }
            if( isset( $_POST['test_work_date'] ) ) {
                update_post_meta( $post_id, 'test_work_date', $_POST['test_work_date'] );
            }
            if( isset( $_POST['test_work_type'] ) ) {
                update_post_meta( $post_id, 'test_work_type', $_POST['test_work_type'] );
            }
        }
    }
}

return new Test_Work_Class();
